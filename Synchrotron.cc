
// Synchrotron utilities:
// exact computation using full formulae, for single electrons energy and integration of input electron spectrum
// for B random and B perpendicular
// total and polarized
//
// Usage illustrated by the test cases below.
//
// to compile, needs GNU Science Library gsl e.g.:
// g++ Synchrotron.cc synchrotron_emissivity.cc  synchrotron_emissivity_B_field.cc B_field_3D_model.cc  -I/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/include -L/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib -lgsl -lgslcblas
// 
// to run:
// setenv LD_LIBRARY_PATH /afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib
// ./a.out

// Andy Strong, aws@mpe.mpg.de
// Garching, 5 Dec 2013
// History
// 20131210 v3 add integration over electron spectrum for B-field model
// 20131209 v2 add integration over electron spectrum for polarized case
// 20131205 v1 first release, integration only for total emissivity

//////////////////////////////////////////////////////////

using namespace std;
#include<iostream>
#include<cmath>

#include"Synchrotron.h"
#include"synchrotron_emissivity.h"
#include"synchrotron_emissivity_B_field.h"

/////////////////////////////////////////////////////////
void Synchrotron:: init(int debug_)
{

  cout<<">> Synchrotron:: init()"<<endl;
  
  initialized=1;
  debug=debug_;

  cout<<"<< Synchrotron:: init()"<<endl;
}
/////////////////////////////////////////////////////////


  


///////////////////////////////////////////////////////////////////////////

  void  Synchrotron:: synch_emiss(valarray<double> Ee,    valarray<double> nu,
                                  valarray<double> fluxe,
                                  double Bperp, double Brand,
                                  valarray<double> &emiss,
                                  int method )

{

  // emiss = total emissivity. For other components use extended routine below.

#include"Constants.h"

  if(debug==1) cout<<">> Synchrotron::synch_emiss()"<< " method="<<method<<endl;

  // assuming energy grid is  logarithmic


  emiss=0.0;

 

  double synch_emissivity_total,    synch_emissivity_regular,
         synch_emissivity_parallel, synch_emissivity_perpendicular, synch_emissivity_random;

  int debug_synchrotron_emissivity=0;

  for (int iEe=0;iEe<Ee.size();iEe++)
  for (int inu=0;inu<nu.size();inu++)
  {
   // erg Hz^-1 s^-1
   synch_emissivity_total=
   synchrotron_emissivity(Ee[iEe]/me, nu[inu], Bperp, Brand,
		        synch_emissivity_regular,      synch_emissivity_parallel, 
                        synch_emissivity_perpendicular,synch_emissivity_random,
                        debug_synchrotron_emissivity);

    emiss[inu] +=  synch_emissivity_total * fluxe[iEe] 
                                          *    Ee[iEe] ;

    if(debug==1) cout<<"synch_emiss: method="<<method<<" nu="<< nu[inu]<<" Ee="<<Ee[iEe]
                     <<"  synch_emissivity_total = "<< synch_emissivity_total<<" emiss="<<emiss[inu]<<endl;
  }


  emiss *= log(Ee[1]/Ee[0]);// log integration

  // erg Hz^-1 s^-1 * cm^-2 sr^-1 s^-1 -> erg Hz^-1 cm^-3 sr^-1 
  emiss  /= c; 


 
  if(debug==1)  cout<<"<< Synchrotron::synch_emiss()"<<endl;
  return;
};

///////////////////////////////////////////////////////////////////////////

  void  Synchrotron:: synch_emiss(valarray<double> Ee,    valarray<double> nu,
                                  valarray<double> fluxe,
                                  double Bperp, double Brand,
                                  valarray<double> &emiss_total,
                                  valarray<double> &emiss_regular,
                                  valarray<double> &emiss_parallel,
                                  valarray<double> &emiss_perpendicular,
                                  valarray<double> &emiss_random,
                                  int method )

{

  // each component of emissivity is computed

#include"Constants.h"

  if(debug==1) cout<<">> Synchrotron::synch_emiss()"<< " method="<<method<<endl;

  // assuming energy grid is  logarithmic


  emiss_total=emiss_regular=emiss_parallel=emiss_perpendicular=emiss_random=0.0;

 

  double synch_emissivity_total,    synch_emissivity_regular,
         synch_emissivity_parallel, synch_emissivity_perpendicular, synch_emissivity_random;

  int debug_synchrotron_emissivity=0;

  for (int iEe=0;iEe<Ee.size();iEe++)
  for (int inu=0;inu<nu.size();inu++)
  {
   // erg Hz^-1 s^-1
   synch_emissivity_total=
   synchrotron_emissivity(Ee[iEe]/me, nu[inu], Bperp, Brand,
		        synch_emissivity_regular,      synch_emissivity_parallel, 
                        synch_emissivity_perpendicular,synch_emissivity_random,
                        debug_synchrotron_emissivity);

    emiss_total        [inu] +=  synch_emissivity_total         * fluxe[iEe] * Ee[iEe] ;
    emiss_regular      [inu] +=  synch_emissivity_regular       * fluxe[iEe] * Ee[iEe] ;
    emiss_parallel     [inu] +=  synch_emissivity_parallel      * fluxe[iEe] * Ee[iEe] ;
    emiss_perpendicular[inu] +=  synch_emissivity_perpendicular * fluxe[iEe] * Ee[iEe] ;
    emiss_random       [inu] +=  synch_emissivity_random        * fluxe[iEe] * Ee[iEe] ;

    if(debug==1) cout<<"synch_emiss: method="<<method<<" nu="<< nu[inu]<<" Ee="<<Ee[iEe]
                     <<"  synch_emissivity_total = "<< synch_emissivity_total<<" emiss_total="<<emiss_total[inu]<<endl;
  }


  emiss_total         *= log(Ee[1]/Ee[0]);// log integration
  emiss_regular       *= log(Ee[1]/Ee[0]);// log integration
  emiss_parallel      *= log(Ee[1]/Ee[0]);// log integration
  emiss_perpendicular *= log(Ee[1]/Ee[0]);// log integration
  emiss_random        *= log(Ee[1]/Ee[0]);// log integration

  // erg Hz^-1 s^-1 * cm^-2 sr^-1 s^-1 -> erg Hz^-1 cm^-3 sr^-1 
  emiss_total          /= c; 
  emiss_regular        /= c; 
  emiss_parallel       /= c; 
  emiss_perpendicular  /= c; 
  emiss_random         /= c; 

 
  if(debug==1)  cout<<"<< Synchrotron::synch_emiss()"<<endl;
  return;
};


/////////////////////////////////////////////////////////
void Synchrotron::test()
{
#include"Constants.h"
  cout<<">> Synchrotron::test()"<<endl;

  cout<<  endl<< "========================== Testing synchrotron emissivity for single electron energy"<<endl<<endl;

  int debug_=0;
  init(debug_);

  // testing synchrotron emissivity for single electron energy, polarized and total

  double Ee,  Eph, eta,  Egamma;
  double nu=1.0e9;// Hz
  Ee=1e4;         // MeV

  double Bperp=1.0e-6;//Gauss
  double Brand=2.0e-6;//Gauss

  double synch_emissivity_total, synch_emissivity_regular,
         synch_emissivity_parallel, synch_emissivity_perpendicular,synch_emissivity_random;



  int debug_synchrotron_emissivity=1;

 synch_emissivity_total=
 synchrotron_emissivity(Ee/me, nu, Bperp, Brand,
		        synch_emissivity_regular,      synch_emissivity_parallel, 
                        synch_emissivity_perpendicular,synch_emissivity_random,
                        debug_synchrotron_emissivity);
  
 cout<<"Synchrotron::test:synchrotron_emissivity: Ee="<<Ee<<" MeV  nu="<<nu<<" Hz  Bperp="<<Bperp<<" Brand="<<Brand <<" Gauss"<<endl
     <<" synch_emissivity_total   ="     <<synch_emissivity_total        <<endl
     <<" synch_emissivity_regular ="     <<synch_emissivity_regular      <<endl
     <<" synch_emissivity_parallel="     <<synch_emissivity_parallel     <<endl
     <<" synch_emissivity_perpendicular="<<synch_emissivity_perpendicular<<endl
     <<" synch_emissivity_random  ="     <<synch_emissivity_random<< " erg Hz-1 s-1"
     <<endl;






//----------------------------------- test synchrotron emissivity for electron spectrum 

  cout<<endl<<" ================= Testing synchrotron emissivity for electron spectrum"<<endl<<endl;

  int nEe    =80 ;   // number of electron energies    NB must be >1 for correct run
  int nnu    =60 ;   // number of frequencies

  double  Eemin       = 100.0;          // electron start  energy in MeV
  double  Eefactor    =   1.1;          // electron        energy grid factor

  double  numin       = 4.08e6;       // synchrotron start frequency in Hz  (chosen to include 408 MHz)
  double  nufactor    = pow(10.,0.1); // synchrotron       frequency factor

  valarray<double> Ee_grid     (nEe);
  valarray<double> nu_grid     (nnu);
  valarray<double> fluxe       (nEe);
  valarray<double> emiss       (nnu);

 valarray<double> emiss_total         (nnu);
 valarray<double> emiss_regular       (nnu);
 valarray<double> emiss_parallel      (nnu);
 valarray<double> emiss_perpendicular (nnu);
 valarray<double> emiss_random        (nnu);

  Bperp= 5.0e-6;//B-field perpendicular to line-of-sight, Gauss
  Brand=10.0e-6;//B-field random component,               Gauss

  int method = 1; // not used in this version
  
  for (int inu=0;inu<nu_grid.size();inu++) nu_grid [inu] = numin * pow(nufactor,inu);

  for (int iEe=0;iEe<Ee_grid.size();iEe++) Ee_grid [iEe] = Eemin * pow(Eefactor,iEe);
  

  // Ackermann etal Fermi-LAT arXiv:1109.052 electrons fit, converting from GeV, m^-2 to MeV, cm^-2 
  double ge=3.19; // electron spectral index
  for (int iEe    =0;iEe    <Ee_grid    .size();iEe++) fluxe       [iEe]    = 2.07e-9* pow(Ee_grid[iEe]/2.e4, -ge   );

  for (int iEe    =0;iEe    <Ee_grid    .size();iEe++) cout<<"electron spectrum: Ee= "    << Ee_grid    [iEe]<<" MeV, flux="<<fluxe[iEe]<<" cm-2 sr-1 s-1 MeV-1"<<endl;




  for (int routine=1;routine<=2;routine++)
  {  

   if(routine==1)
   {
  // in class Synchrotron
  // synchrotron spectrum for electron spectrum, total only
   synch_emiss(Ee_grid,  nu_grid, fluxe,  Bperp, Brand, emiss, method );

  // synchrotron spectrum for electron spectrum, all components
   synch_emiss(Ee_grid,  nu_grid, fluxe,  Bperp, Brand, emiss_total, emiss_regular,emiss_parallel,emiss_perpendicular,emiss_random, method );

   }


   if(routine==2)
   {
   // in synchrotron_emissivity.cc
   // synchrotron spectrum for electron spectrum, total only
   synchrotron_emissivity(Ee_grid,  nu_grid, fluxe,  Bperp, Brand, emiss, debug );

  // synchrotron spectrum for electron spectrum, all components
   synchrotron_emissivity(Ee_grid,  nu_grid, fluxe,  Bperp, Brand, emiss_total, emiss_regular,emiss_parallel,emiss_perpendicular,emiss_random, debug );
   }



   // sample application to a volume:  "cloud"

   // convert to brightness temperature
   valarray<double> T(nnu);
   double I_to_Tb=c*c/ (2.* k);      // intensity to brightness temp: Tb= I_to_Tb * I/nu^2;
 
   double cloud_radius=10.; // pc
   double cloud_volume=4./3.*pi*pow(cloud_radius*3.01e18, 3); // cm^3
   double cloud_distance=1000.;// pc
   

   valarray<double> cloud_flux(nnu);

   cloud_flux=cloud_volume * emiss * pow(cloud_distance * 3.08e18, -2);// erg  cm^-2 Hz^-1 NB 4pi*emiss/(4pi dist^2).
   cloud_flux*=1e-7*1e4;                                               // W m^-2 Hz^-1
   cloud_flux/=1e-26;                                                  // Jy


   valarray<double> I(nnu);
   I= emiss * 2.* cloud_radius * 3.08e18;// erg  cm^-2 Hz^-1 sr^-1
   T = I * I_to_Tb  / pow(nu_grid,2.0);// convert to brightness temperature in K
   //   I*=1e-7*1e4; // W m^-2 Hz^-1  sr^-1
  

   cout<<endl<<"total emissivity and fluxes"<<endl<<endl;

   for (int inu=0;inu<nu_grid.size();inu++)   cout<<" Bperp="<<Bperp<< " Brand="<<Brand<< " G"
                                                  <<" nu= "<< nu_grid[inu]<<" Hz"                                              
                                                  <<" cloud radius=" <<cloud_radius<<" pc"
                                                  <<" distance="<<cloud_distance<<" pc"
                                                  <<" emiss="<<emiss[inu]<<" erg Hz^-1 cm^-3 sr^-1"
                                                  <<" I="<<I[inu]<<"  erg  cm^-2 Hz^-1 sr^-1"
                                                  <<" T="<<T[inu]<<" K"
                                                  <<" flux="<<cloud_flux[inu]<<" Jy"

                                                  <<endl;

   cout<<endl<<"all emissivity components"<<endl<<endl;

  for (int inu=0;inu<nu_grid.size();inu++)   cout<<" Bperp="<<Bperp<< " Brand="<<Brand<< " G"
                                                  <<" nu= "<< nu_grid[inu]<<" Hz"                                              
                     
                                                  <<" emiss_regular="      <<emiss_regular      [inu]
                                                  <<" emiss_parallel="     <<emiss_parallel     [inu]
                                                  <<" emiss_perpendicular="<<emiss_perpendicular[inu]
                                                  <<" emiss_random="       <<emiss_random       [inu]
                                                  <<" emiss_total="         <<emiss_total       [inu]
                                                  <<" erg Hz^-1 cm^-3 sr^-1"
                                                
                                                  <<endl;



  }// routine
 

  // use a B-field model

  {
    cout<<endl<<   "==================== Testing synchrotron emissivity for single electron energy and a B-field model"<<endl<<endl;





  // name and parameters of B-field model, illustrative values. See B_field_models.cc for details of each model.

  string B_field_name="Sun_ASS_RING_2";

  vector<double> parameters; parameters.resize(10);

  parameters[0]= -12.0   ;// pitch-angle, degrees
  parameters[1]=   2.0e-6;// B0 for regular, Gauss
  parameters[2]=  10.0   ;// R-scale for regular, kpc
  parameters[3]=   1.0   ;// z-scale for regular, kpc
  parameters[4]=  3.0e-6 ;// B0 for random, Gauss
  parameters[5]=  20.0   ;// R-scale for random, kpc
  parameters[6]=   2.0   ;// z-scale for random, kpc
  parameters[7]=   1.0e-6;// B0 for halo field, Gauss
  parameters[8]=   5.0   ;// Rc kpc: regular B=Bc inside Rc
  parameters[9]=   2.0e-6;// Bc: regular B inside Rc, Gauss

  double x,y,z,x0,y0,z0;

  x =1.;y =2.;z =3.; // position at which emissivity is to be computed, kpc, Galactic system centred at (0,0,0), solar position = (8.5,0,0)
  x0=0.;y0=0.;z0=0.; // position of observer                                 ditto


  int synchrotron_options=0; // not yet used
  
  double I, Q, U;

  synch_emissivity_total=
  synchrotron_emissivity_B_field( Ee/me,  nu,
                                       B_field_name, parameters,
                                       x, y, z,
                                       synchrotron_options,
                                       x0, y0, z0,
                                       synch_emissivity_regular,       synch_emissivity_parallel,
                                       synch_emissivity_perpendicular, synch_emissivity_random,
			   	       I, Q, U,
                                       debug_synchrotron_emissivity); 

 cout<<"Synchrotron::test:synchrotron_emissivity_B_field: Ee="<<Ee<<" MeV  nu="<<nu<<" Hz  B_field_name="<< B_field_name<<endl
     <<" synch_emissivity_total="        <<synch_emissivity_total        <<endl
     <<" synch_emissivity_regular="      <<synch_emissivity_regular      <<endl
     <<" synch_emissivity_parallel="     <<synch_emissivity_parallel     <<endl
     <<" synch_emissivity_perpendicular="<<synch_emissivity_perpendicular<<endl
     <<" synch_emissivity_random="       <<synch_emissivity_random       <<endl
     <<" Stokes I="<<I    <<"  Q="<<Q    <<"  U="<<U << "  erg Hz-1 cm-3 sr-1 s-1"
     <<endl;


 {
   valarray<double> synch_emissivity_total        (nnu);
   valarray<double> synch_emissivity_regular      (nnu);
   valarray<double> synch_emissivity_parallel     (nnu);
   valarray<double> synch_emissivity_perpendicular(nnu);
   valarray<double> synch_emissivity_random       (nnu);
   valarray<double>                              I(nnu), Q(nnu), U(nnu);



   // integration over electron spectrum, routine without Stokes parameters

 synchrotron_emissivity_B_field( Ee_grid,  nu_grid, fluxe,
                                       B_field_name, parameters,
                                       x, y, z,
                                       synchrotron_options,
                                       x0, y0, z0,
				       synch_emissivity_total,
                                       synch_emissivity_regular,       synch_emissivity_parallel,
                                       synch_emissivity_perpendicular, synch_emissivity_random,
			   	       debug_synchrotron_emissivity); 


 cout<<"Synchrotron::test:synchrotron_emissivity_B_field:  B_field_name="<< B_field_name<<endl;

 for(int inu=0;inu<nnu;inu++)
 {
 cout<<"B_field, integration over electron spectrum.  nu="<<nu_grid[inu]<<" Hz"
     <<" synch_emissivity_total="        <<synch_emissivity_total[inu]         
     <<" synch_emissivity_regular="      <<synch_emissivity_regular [inu]     
     <<" synch_emissivity_parallel="     <<synch_emissivity_parallel [inu]     
     <<" synch_emissivity_perpendicular="<<synch_emissivity_perpendicular[inu] 
     <<" synch_emissivity_random="       <<synch_emissivity_random [inu]   
     <<"  erg Hz-1 cm-3 sr-1 s-1"   
     <<endl;
}


 // integration over electron spectrum, routine with Stokes parameters

synchrotron_emissivity_B_field( Ee_grid,  nu_grid, fluxe,
                                       B_field_name, parameters,
                                       x, y, z,
                                       synchrotron_options,
                                       x0, y0, z0,
				       synch_emissivity_total,
                                       synch_emissivity_regular,       synch_emissivity_parallel,
                                       synch_emissivity_perpendicular, synch_emissivity_random,
				       I, Q, U,			   	    
                                       debug_synchrotron_emissivity); 


 cout<<"Synchrotron::test:synchrotron_emissivity_B_field:  B_field_name="<< B_field_name<<endl;

 for(int inu=0;inu<nnu;inu++)
 {
 cout<<"B_field, integration over electron spectrum.  nu="<<nu_grid[inu]<<" Hz"
     <<" synch_emissivity_total="        <<synch_emissivity_total[inu]         
     <<" synch_emissivity_regular="      <<synch_emissivity_regular [inu]     
     <<" synch_emissivity_parallel="     <<synch_emissivity_parallel [inu]     
     <<" synch_emissivity_perpendicular="<<synch_emissivity_perpendicular[inu] 
     <<" synch_emissivity_random="       <<synch_emissivity_random [inu]      
     <<" Stokes I="<<I[inu]     <<"  Q="<<Q[inu]     <<"  U="<<U[inu]  
     << "  erg Hz-1 cm-3 sr-1 s-1"
     <<endl;
  }




 } // energy integration block

  } //B-field models block




  cout<<"<< Synchrotron::test()"<<endl;
  return;
}

///////////////////////////// Main program

int main()
{

  Synchrotron synchrotron;

  synchrotron.test();

  return 0;
}


