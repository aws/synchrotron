using namespace std;
#include<vector>
#include <valarray>

class Synchrotron
{
public:
  void   init(int debug);
  

  void   synch_emiss(valarray<double> Ee,     valarray<double> Egamma,
                  valarray<double> fluxe, 
		  double Bperp, double Brand,
                  valarray<double> &emiss,
                  int method );


  void  synch_emiss(valarray<double> Ee,    valarray<double> nu,
                                  valarray<double> fluxe,
                                  double Bperp, double Brand,
                                  valarray<double> &emiss_total,
                                  valarray<double> &emiss_regular,
                                  valarray<double> &emiss_parallel,
                                  valarray<double> &emiss_perpendicular,
                                  valarray<double> &emiss_random,
				  int method );

  void   test();

private:
 
  double constant;
  int    initialized;
  int    debug;
};
