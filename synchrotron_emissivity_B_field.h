using namespace std;
#include <valarray>

double synchrotron_emissivity_B_field( double gamma, double nu,
                                       const std::string &name, const std::vector<double> &parameters,
                                       double x, double y, double z,
                                       int options,
                                       double x0, double y0, double z0,
                                       double     &synch_emissivity_reg, double &synch_emissivity_par, double &synch_emissivity_perp, double &synch_emissivity_random,
                                       int debug );



double synchrotron_emissivity_B_field( double gamma, double nu,
                                       const std::string &name, const std::vector<double> &parameters,
                                       double x, double y, double z,
                                       int options,
                                       double x0, double y0, double z0,
                                       double     &synch_emissivity_reg, double &synch_emissivity_par, double &synch_emissivity_perp, double &synch_emissivity_random,
                                       double     &I, double     &Q, double &U,
                                       int debug ); //AWS20100706


void synchrotron_emissivity_B_field(  valarray<double> Ee,  valarray<double> nu,
                                      valarray<double> fluxe,
                                      const std::string &name, const std::vector<double> &parameters,
                                      double x, double y, double z,
                                      int options,
                                      double x0, double y0, double z0,
                                      valarray<double> &synch_emissivity_total,
                                      valarray<double> &synch_emissivity_reg,
                                      valarray<double> &synch_emissivity_par,
                                      valarray<double> &synch_emissivity_perp,
                                      valarray<double> &synch_emissivity_random,
			              int debug ); //AWS20131209


void synchrotron_emissivity_B_field(  valarray<double> Ee,  valarray<double> nu,
                                      valarray<double> fluxe,
                                      const std::string &name, const std::vector<double> &parameters,
                                      double x, double y, double z,
                                      int options,
                                      double x0, double y0, double z0,
                                      valarray<double> &synch_emissivity_total,
                                      valarray<double> &synch_emissivity_reg,
                                      valarray<double> &synch_emissivity_par,
                                      valarray<double> &synch_emissivity_perp,
                                      valarray<double> &synch_emissivity_random,
                                      valarray<double> &I, valarray<double> &Q, valarray<double> &U,
			              int debug ); //AWS20131209
