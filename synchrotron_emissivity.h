using namespace std;
#include<vector>
#include <valarray>


double synchrotron_emissivity(double gamma, double nu,  double Bperp, double Brand,
                              double &synch_emissivity_regular,       double &synch_emissivity_parallel, double &synch_emissivity_perpendicular,
                              double &synch_emissivity_random,
                              int debug=0 );

//AWS20131209 versions to integrate over electron spectrum, for grid of frequencies

void  synchrotron_emissivity(valarray<double> Ee,  valarray<double> nu,
                  valarray<double> fluxe, 
		  double Bperp, double Brand,
                  valarray<double> &emiss,
                  int debug=0 );


void  synchrotron_emissivity(valarray<double> Ee,  valarray<double> nu,
                                  valarray<double> fluxe,
                                  double Bperp, double Brand,
                                  valarray<double> &emiss_total,
                                  valarray<double> &emiss_regular,
                                  valarray<double> &emiss_parallel,
                                  valarray<double> &emiss_perpendicular,
                                  valarray<double> &emiss_random,
				  int debug=0 );
