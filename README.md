Routines to compute sychrotron from relativistic electrons, including polarization.
Andrew Strong, aws@mpe.mpg.de

20 June 2017 Starting from v3 at sourceforge 
https://sourceforge.net/projects/galpropsynchrotron
where it was maintained, now this gitlab will host this project.

Copy of description from sourceforge:





The GALPROP package computes cosmic-ray propagation and gamma-ray
 and other emissions. 
The extension to synchrotron radiation including polarized emission,
 3D magnetic field models, and free-free emission and absorption,
 is described in  http://arxiv.org/abs/1309.2947 ,
 published version: http://mnras.oxfordjournals.org/content/436/3/2127 .

To support the paper with detailed information, 
here is a full version with polarization, free-free emission and absorption.
https://gitlab.mpcdf.mpg.de/aws/galprop

* Synchrotron_v3.tar: standalone computation of synchrotron emissivity including polarization,
 integration over electron spectrum, B-field models with Stokes.

See also http://www.mpe.mpg.de/~aws/propagate.html 



Features and history


 5 Dec 2013: Synchrotron_v1.tar: standalone computation of synchrotron emissivity including polarization.
 Integration over electron energy spectrum.
 9 Dec 2013: Synchrotron_v2.tar: add integration over electron spectrum for polarized case
10 Dec 2013: Synchrotron_v3.tar: add integration over electron spectrum for B-field models, with Stokes parameters.
20 Jun 2017: moved to this gitlab distribution for future work



